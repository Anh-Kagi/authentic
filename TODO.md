# TODO list:

- allow non-logged player to be teleported (can be useful)
- handle db errors on player's connection (if db unavailable the kicking timeout won't be set)
- non-logged players are transported to a lobby until they log in
- add a command to change the player's username
