# Authentic

Authentic is an authentication plugin for Sponge for minecraft servers.

# User interface

The plugin offers the user 3 commands:
- `/register`
- `/login`
- `/logged`

These three commands allows the user to authenticate to the minecraft server.    
The details of the commands are available in the [wiki](https://framagit.org/Anh-Kagi/authentic.wiki.git).    

# Database

The player's data are stored in a SQL database (H2 and MySQL are supported), it contains:
- the player's username.
- the player's password hash.
- the player's last logged timestamp.
- the player's last logged ip.

The database has the following structure:

```SQL
CREATE TABLE IF NOT EXISTS `AUTHENTIC` (
    ID INTEGER AUTO_INCREMENT NOT NULL,
    USERNAME VARCHAR(16) NOT NULL, -- max length for mc username
    HASH VARCHAR(88) DEFAULT '' NOT NULL, -- max length for base64 SHA512 hashed string
    LASTIP VARCHAR(45) DEFAULT '' NOT NULL,
    LASTLOG BIGINT DEFAULT 0 NOT NULL,
    PRIMARY KEY (ID),
    UNIQUE (username)
);
```

### Migrations:

#### 1.4.2: IPv6 support

You will need to update the `lastIP` field in your database. If you use H2 as your database engine, you might need an [H2 db manager](http://h2database.com/html/download.html).

- upward migration (MySQL):
```SQL
ALTER TABLE `authentic` MODIFY `lastIP` VARCHAR(45) DEFAULT '' NOT NULL;
```
- downward migration (MySQL):
```SQL
ALTER TABLE `authentic` MODIFY `lastIP` VARCHAR(15) DEFAULT '' NOT NULL;
```

# Configuration file

For now, only 8 rules are available, they modifies the plugins behaviour to player's interactions.    
The rules available are:

- `immobilizePlayers`    
It allows to immobilize the players that are not yet logged in (they cannot move or interact with the world).

- `allowRegister`    
It enables (or disables) the `/register` command.

- `timeOutDelay`    
It sets the amount of time to wait for a player to log in before being kicked out.

- `minimumPwdSize`    
It indicates the minimum length a password must be.

- `doKickOnWrongPwd`    
It sets the behaviour on a wrong given password (if `true`, the player is kicked out).

- `logCacheDelay`    
It indicates the amount of time a player can wait to re-login from the same ip.

- `invulnerablePlayers`    
It indicates if non-logged players can be hurted.

- `allowCommands`    
If true, non-logged players won't be able to use commands, except Authentic's `/login`, `/register` and `/logged`.

- `allowDropItems`    
If true, non-logged players won't be able to drop items.

The configuration file also stores the localization (L10n) strings.

For more information, check the [wiki](https://framagit.org/Anh-Kagi/authentic.wiki.git).
