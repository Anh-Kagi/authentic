package org.framagit.anhkagi.authentic.util;

import ninja.leaping.configurate.ConfigurationNode;

/**
 * The class containing all the strings displayed by the plugin.
 * 
 * @author Anh-Kagi
 */
public class L10n {
	/**
	 * Loads the strings from the L10n node of the config file.
	 * 
	 * @param node the configuration node to load
	 * 
	 * @see ConfigurationNode
	 */
	public static void load(ConfigurationNode node) {
		preInit = node.getNode("server", "preInit").getString();
		init = node.getNode("server", "init").getString();
		registeringCommand = node.getNode("server", "registeringCommand").getString();
		skipRegistration = node.getNode("server", "skipRegistration").getString();
		dbPointsTo = node.getNode("server", "dbPointsTo").getString();
		
		playerJoin = node.getNode("user", "logs", "playerJoin").getString();
		playerQuit = node.getNode("user", "logs", "playerQuit").getString();
		playerJustLogged = node.getNode("user", "logs", "playerJustLogged").getString();
		playerJustRegistered = node.getNode("user", "logs", "playerJustRegistered").getString();
		playerJustChangedPwd = node.getNode("user", "logs", "playerJustChangedPwd").getString();
		dbQuery = node.getNode("user", "logs", "dbQuery").getString();
		dbSync = node.getNode("user", "logs", "dbSync").getString();
		
		welcomeLog = node.getNode("user", "messages", "welcomeLog").getString();
		welcomeReg = node.getNode("user", "messages", "welcomeReg").getString();
		welcomeAlreadyLog = node.getNode("user", "messages", "welcomeAlreadyLog").getString();
		timeOut = node.getNode("user", "messages", "timeOut").getString();
		logStatusTrue = node.getNode("user", "messages", "logStatusTrue").getString();
		logStatusFalse = node.getNode("user", "messages", "logStatusFalse").getString();
		playerLogStatusTrue = node.getNode("user", "messages", "playerLogStatusTrue").getString();
		playerLogStatusFalse = node.getNode("user", "messages", "playerLogStatusFalse").getString();
		wrongPassword = node.getNode("user", "messages", "wrongPassword").getString();
		passwordSet = node.getNode("user", "messages", "passwordSet").getString();

		warnPwdDiff = node.getNode("warnings", "warnPwdDiff").getString();
		warnNotRegistered = node.getNode("warnings", "warnNotRegistered").getString();
		warnNotLogged = node.getNode("warnings", "warnNotLogged").getString();
		warnTargetNotLogged = node.getNode("warnings", "warnTargetNotLogged").getString();
		
		commandLoggedDesc = node.getNode("descriptions", "commandLoggedDesc").getString();
		commandLoginDesc = node.getNode("descriptions", "commandLoginDesc").getString();
		commandRegisterDesc = node.getNode("descriptions", "commandRegisterDesc").getString();
		
		errorUnableToLog = node.getNode("errors", "errorUnableToLog").getString();
		errorNotLogged = node.getNode("errors", "errorNotLogged").getString();
		errorPwdTooShort = node.getNode("errors", "errorPwdTooShort").getString();
		errorDB = node.getNode("errors", "errorDB").getString();
		errorDBFile = node.getNode("errors", "errorDBFile").getString();

		consoleWantsToLog = node.getNode("easteregg", "consoleWantsToLog").getString(null);
		if (consoleWantsToLog == null) consoleWantsToLog = "You will never be like them...";
		commandblockWantsToLog = node.getNode("easteregg", "commandblockWantsToLog").getString(null);
		if (commandblockWantsToLog == null) commandblockWantsToLog = "Sorry, but I don't see you in my database, are you sure to be real?";
	}
	
	/**
	 * The string to display when the configuration have been loaded.<br>
	 * 
	 * Its default value is "Loaded data from config file".<br>
	 * 
	 * <strong>Cannot be modified by the configuration file</strong>
	 */
	public static final String configLoaded = "Loaded data from config file";
	/**
	 * The string to display when the default configuration have been loaded.<br>
	 * 
	 * Its default value is "Loaded default data from default config file".<br>
	 * 
	 * <strong>Cannot be modified by the configuration file</strong>
	 */
	public static final String configDefaultLoaded = "Loaded default data from default config file";
	/**
	 * The string to display when the configuration couldn't be loaded.<br>
	 * 
	 * Its default value is "Couldn't load configuration file!".<br>
	 * 
	 * <strong>Cannot be modified by the configuration file</strong>
	 */
	public static final String errorConfigLoad = "Couldn't load configuration file!";
	/**
	 * The string to display when the configuration couldn't be saved.<br>
	 * 
	 * Its default value is "Couldn't save configuration file!".<br>
	 * 
	 * <strong>Cannot be modified by the configuration file</strong>
	 */
	public static final String errorConfigSave = "Couldn't save configuration file!";
	/**
	 * The string to display when the configuration have been saved.<br>
	 * 
	 * Its default value is "Configuration saved!".<br>
	 * 
	 * <strong>Cannot be modified by the configuration file</strong>
	 */
	public static final String configSaved = "Configuration saved!";
	/**
	 * The string displayed after a succesful verification of the configuration file.<br>
	 * 
	 * Its default value is "Configuration file is valid!".<br>
	 * 
	 * <strong>Cannot be modified by the configuration file</strong>
	 */
	public static final String configValid = "Configuration file is valid!";
	/**
	 * The string displayed after an unsuccesful verification of the configuration file.<br>
	 * 
	 * Its default value is "Configuration file is invalid!".<br>
	 * 
	 * <strong>Cannot be modified by the configuration file</strong>
	 */
	public static final String configInvalid = "Configuration file is invalid!";
	/**
	 * The string displayed for server's shutdown when configuration file is invalid.<br>
	 * 
	 * Its default value is "Authentic couldn't load because of an invalid configuration file".<br>
	 * 
	 * <strong>Cannot be modified by the configuration file</strong>
	 */
	public static final String configInvalidShutdown = "Authentic couldn't load because of an invalid configuration file";
	
	// plugin loading
	/**
	 * The string to display when the preInit event has been emitted.
	 * 
	 * Its default value is "pre-init Authentic...".<br>
	 */
	public static String preInit;
	/**
	 * The string to display when the init event has been emitted.
	 * 
	 * Its default value is "init Authentic...".<br>
	 */
	public static String init;
	/**
	 * The string to display when the plugin is registering commands.
	 * 
	 * Its default value is "Regitering command %s".<br>
	 * 
	 * The command name is passed through %s.
	 */
	public static String registeringCommand;
	/**
	 * The string to display when the plugin skips the registration of /register
	 * 
	 * Its default value is "skipping /register registration as specified in config".<br>
	 */
	public static String skipRegistration;
	/**
	 * The string displayed in preinit to show the database's uri.<br>
	 * 
	 * Its default value is "Database points to: %s".<br>
	 * 
	 * The jdbc string is passed through %s.
	 */
	public static String dbPointsTo;
	
	// player's strings
	//		logs
	/**
	 * The string to display when a player is joining.<br>
	 * 
	 * Its default value is "New player: %s".<br>
	 * 
	 * The player's name is passed through %s.
	 */
	public static String playerJoin;
	/**
	 * The string to display when a player quits.<br>
	 * 
	 * Its default value is "Player Quitted: %s".<br>
	 * 
	 * The player's name is passed through %s.
	 */
	public static String playerQuit;
	/**
	 * The string to display when a player has logged in using /login.<br>
	 * 
	 * Its default value is "New player: %s".<br>
	 * 
	 * The player's name is passed through %s.
	 */
	public static String playerJustLogged;
	/**
	 * The string to display when  a player has registered using /register.<br>
	 * 
	 * Its default value is "%s logged in!".<br>
	 * 
	 * The player's name is passed through %s.
	 */
	public static String playerJustRegistered;
	/**
	 * The string to display when a player changed his password using /register.<br>
	 * 
	 * Its default value is "%s logged in!".<br>
	 * 
	 * The player's name is passed through %s.
	 */
	public static String playerJustChangedPwd;
	/**
	 * The string to log when the data is retrieved from the database.<br>
	 * 
	 * Its default value is "Querying user %s".<br>
	 * 
	 * The player's name is passed through %s.
	 */
	public static String dbQuery;
	/**
	 * The string to log when the data has been sent to the database.<br>
	 * 
	 * Its default value is "Synced user %s".<br>
	 * 
	 * The player's name is passed through %s.
	 */
	public static String dbSync;
	//		messages/command's results
	/**
	 * The string to display when the player joined and has already registered.<br>
	 * 
	 * Its default value is "Use /login to authenticate yourself.".
	 */
	public static String welcomeLog;
	/**
	 * The string to display when the player joined and hasn't registered already.<br>
	 * 
	 * Its default value is "Use /register to authenticate yourself.".
	 */
	public static String welcomeReg;
	/**
	 * The string to display when the player joined and hasn't registered already.<br>
	 * 
	 * Its default value is "You were logged in with this ip in the last %s seconds.<br>
	 * 
	 * The value of the "logCacheDelay" rule is passed through %s.
	 */
	public static String welcomeAlreadyLog;
	/**
	 * The string to display when the player hadn't logged in time.<br>
	 * 
	 * Its default value is "You didn't logged in time!".
	 */
	public static String timeOut;
	/**
	 * The string to display when the player asked his status and was logged in.<br>
	 * 
	 * Its default value is "You are logged!".
	 */
	public static String logStatusTrue;
	/**
	 * The string to display when the player asked his status and wasn't logged in.<br>
	 * 
	 * Its default value is "You are not logged!".
	 */
	public static String logStatusFalse;
	/**
	 * The string to display when a player's status has been asked and was logged in.<br>
	 * 
	 * Its default value is "%s is logged!".<br>
	 * 
	 * The player's name is passed through %s.
	 */
	public static String playerLogStatusTrue;
	/**
	 * The string to display when a player's status has been asked and wasn't logged in.<br>
	 * 
	 * Its default value is "%s isn't logged!".<br>
	 * 
	 * The player's name is passed through %s.
	 */
	public static String playerLogStatusFalse;
	/**
	 * The string to display when a player tried /login with a wrong passord.<br>
	 * 
	 * Its default value is "The entered password is incorrect!".
	 */
	public static String wrongPassword;
	/**
	 * The string to display when a player changed his password using /register.<br>
	 * 
	 * Its default value is "Password set!".
	 */
	public static String passwordSet;
	// 		warns
	/**
	 * The string displayed when the player ran /register but gave two different passwords.<br>
	 * 
	 * Its default value is "You entered two different passwords!".
	 */
	public static String warnPwdDiff;
	/**
	 * The string displayed when the player tried to /login but wasn't registered.<br>
	 * 
	 * Its default value is "You are not registered! Use /register".
	 */
	public static String warnNotRegistered;
	/**
	 * The string to display when the player attempts to move or interact without being logged in.<br>
	 * 
	 * Its default value is "You must login before doing anything!".
	 */
	public static String warnNotLogged;
	/**
	 * The string displayed when a player tris to hit a non-logged player.<br>
	 * 
	 * Its default value is "The targetted player isn't logged".<br>
	 */
	public static String warnTargetNotLogged;
	
	// command's descriptions
	/**
	 * This string is the /logged description.<br>
	 * 
	 * Its default value is "Get the player's logged status".
	 */
	public static String commandLoggedDesc;
	/**
	 * This string is the /login description.<br>
	 * 
	 * Its default value is "Allows to authenticate to the server".
	 */
	public static String commandLoginDesc;
	/**
	 * This string is the /register description.<br>
	 * 
	 * Its default value is "Allows to register or to change the password".
	 */
	public static String commandRegisterDesc;
	
	// easter eggs
	/**
	 * The string displayed when the console ran /login or /register.<br>
	 * 
	 * Its default value is "You will never be like them...".
	 */
	public static String consoleWantsToLog;
	/**
	 * The string displayed when a command block ran /login or /register.<br>
	 * 
	 * Its default value is "Sorry, but I don't see you in my database, are you sure to be real?".
	 */
	public static String commandblockWantsToLog;
	
	// errors
	/**
	 * The string displayed when something ran /login or /register but wasn't a player.<br>
	 * 
	 * Its default value is "You don't seem to be able to login".
	 */
	public static String errorUnableToLog;
	/**
	 * The string displayed when a registered player tried /register without being logged in.<br>
	 * 
	 * Its default value is "You are not connected!\nYou need to be connected to change your password!".
	 */
	public static String errorNotLogged;
	/**
	 * The string displayed when a player tried to register with a too short password.<br>
	 * 
	 * Its default value is "You entered a password that was too short!\nThe minimum length is %s".<br>
	 * 
	 * The minimum password length is passed through %s.
	 */
	public static String errorPwdTooShort;
	/**
	 * The string displayed when Authentic tries to retrieve player's data from the database, but fails to.<br>
	 * 
	 * Its default value is "An issue happened with the database, the command couldn't succeed."<br>
	 */
	public static String errorDB;
	/**
	 * The string displayed when Authentic tries to copy the H2 db file to the configuration directory, but for a reason, fails to.<br>
	 * 
	 * Its default value is "Authentic couldn't create the database file"<br>
	 */
	public static String errorDBFile;
}
