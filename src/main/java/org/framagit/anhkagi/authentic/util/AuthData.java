package org.framagit.anhkagi.authentic.util;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.Instant;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.framagit.anhkagi.authentic.Authentic;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.plugin.PluginContainer;
import org.spongepowered.api.scheduler.Task;
import org.spongepowered.api.service.sql.SqlService;

/**
 * The class that wraps the player's data such the logged status or the password.
 * 
 * @author Anh-Kagi
 * 
 */
public class AuthData {
	public static PluginContainer plugin;
	
	/**
	 * The SqlService class to use (here, it's furnished by Sponge).<br>
	 * 
	 * @see SqlService
	 */
	public static SqlService sql = Sponge.getServiceManager().provide(SqlService.class).get();
	
	/**
	 * The jdbc uri to use to connect to the database.<br>
	 * 
	 * @see Connection
	 */
	public static String uri;
	
	/**
	 * the table name to use in the database.<br>
	 */
	public static String table;
	
	private static Map<String, AuthPlayer> players = new HashMap<String, AuthPlayer>();
	
	private static AuthPlayer getData(String name) {
		if (!players.containsKey(name)) { // creates an instance if it doesn't exists already
			AuthPlayer player;
			if ((player = query(name)) != null)
				players.put(name, player);
			else
				return null;
		}
		return players.get(name);
	}
	
	private AuthPlayer getData() {
		return getData(getName());
	}
	
	private static AuthPlayer query(String name) {
		Authentic.log(String.format(L10n.dbQuery, name));
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			conn = sql.getDataSource(uri).getConnection();
			stmt = conn.prepareStatement("SELECT `HASH`,`LASTIP`,`LASTLOG` FROM `" + table + "` WHERE `USERNAME`='" + name + "';");
			rs = stmt.executeQuery();
			
			if (rs.first())
				return new AuthPlayer(rs.getString("HASH"), false, rs.getString("LASTIP"), rs.getLong("LASTLOG"));
			else {
				PreparedStatement istmt = null;
				try {
					istmt = conn.prepareStatement("INSERT INTO `" + table + "` (`USERNAME`, `HASH`, `LASTLOG`, `LASTIP`) VALUES ('" + name + "', '', 0, '');");
					istmt.executeUpdate();
					
					return new AuthPlayer();
				} catch (Exception e) {
					e.printStackTrace();
					return null;
				} finally {
					try {
						if (istmt != null) istmt.close();
					} catch (Exception e) {
						// nothing we can do
					}
				}
			}
		} catch (Exception e) {
			Authentic.warn(L10n.errorDB);
			return null;
		} finally {
			try {
				if (conn != null) conn.close();
				if (stmt != null) stmt.close();
				if (rs != null) rs.close();
			} catch (SQLException e) {
				// nothing we can do
			}
		}
	}
	
	/**
	 * Retrieves the player's data.<br>
	 * 
	 * @param name The player's name.
	 * 
	 * @return true if the data has been retrieved correctly, false otherwise.
	 */
	public static boolean retrieve(String name) {
		if (getData(name) != null) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Syncs the player's data to the database.<br>
	 * 
	 * If successful, the data is removed from the memory.<br>
	 * 
	 * @param name The player's name.
	 */
	public static void save(String name) {
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			conn = sql.getDataSource(uri).getConnection();
			AuthPlayer player = getData(name);
			stmt = conn.prepareStatement("UPDATE `"+table+"` SET `HASH`='"+player.getHash()+"',`LASTLOG`="+player.getLastLog()+",`LASTIP`='"+player.getLastIP()+"' WHERE `USERNAME`='"+name+"';");
			
			stmt.executeUpdate();
			Authentic.log(String.format(L10n.dbSync, name));
			players.remove(name); // removing player from memory (players are saved when they quit)
		} catch (Exception e) {
			Authentic.warn(L10n.errorDB);
		} finally {
			try {
				if (conn != null) conn.close();
				if (stmt != null) stmt.close();
			} catch (SQLException e) {
				// nothing we can do
			}
		}
	}
	
	private static class AuthPlayer {
		private boolean logged;
		
		public boolean getLogged() {
			return logged;
		}
		
		public void setLogged(boolean logged) {
			this.logged = logged;
		}
		
		private Task TIMEOUT;
		
		public Task getTimeout() {
			return TIMEOUT;
		}
		
		public void setTimeout(Task timeout) {
			this.TIMEOUT = timeout;
		}
		
		public void setTimeout(Runnable func, int dt) {
			setTimeout(Task.builder().execute(func).delay(dt, TimeUnit.SECONDS).submit(plugin));
		}
		
		public void interruptTimeout() {
			if (TIMEOUT != null)
				TIMEOUT.cancel();
		}
		
		private String hash;
		
		public String getHash() {
			return hash;
		}
		
		public void setHash(String hash) {
			this.hash = hash;
		}
		
		public void setPwd(String pwd) {
			setHash(Password.hash(pwd));
		}
		
		private long lastLog;
		
		public long getLastLog() {
			return lastLog;
		}
		
		public void setLastLog(long lastLog) {
			this.lastLog = lastLog;
		}
		
		public void setLastLog(Instant lastLog) {
			setLastLog(lastLog.getEpochSecond());
		}
		
		private String lastIP;
		
		public String getLastIP() {
			return lastIP;
		}
		
		public void setLastIP(String lastIP) {
			this.lastIP = lastIP;
		}
		
		public AuthPlayer(String hash, boolean logged, String lastIP, long lastLog) {
			setHash(hash);
			setLogged(logged);
			setLastIP(lastIP);
			setLastLog(lastLog);
		}
		
		public AuthPlayer(String hash, boolean logged) {
			this(hash, logged, "", 0);
		}
		
		public AuthPlayer(String hash) {
			this(hash, false);
		}
		
		public AuthPlayer() {
			this("");
		}
	}
	
	private String name;
	
	private void setName(String name) {
		this.name = name;
	}
	
	/**
	 * The getter for the targetted player's name.
	 * 
	 * @return the player's name
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Returns a wrapper (if I can call this so) for the db data.<br>
	 * 
	 * It does nothing but sets the player's name.
	 *  
	 * @param name the name of the player to target
	 */
	public AuthData(String name) {
		setName(name);
	}
	
	/**
	 * "Shortcut" to skip the instantiation in the code (the instantiation is made directly in the function).
	 * 
	 * @param name the player's username
	 * 
	 * @return an AuthData instance
	 * 
	 * @see AuthData#AuthData(String)
	 */
	public static AuthData getPlayer(String name){
		return new AuthData(name);
	}
	
	/**
	 * Stores the hashed value of the given password.
	 * 
	 * @param password the password to hash
	 * 
	 * @see #setHash(String)
	 * @see #getHash()
	 * @see Password#hash(String)
	 */
	public void setPwd(String password) {
		getData().setPwd(password);
	}
	
	/**
	 * Stores the hash in the db.<br>
	 * It must be represented using base64 and hashed using SHA512 (the length of the string should therefore be 88 characters).<br>
	 * 
	 * @param hash the hash to store.
	 * 
	 * @see Password#hash(String)
	 * @see #setPwd(String)
	 * @see #getHash()
	 */
	public void setHash(String hash) {
		getData().setHash(hash);
	}
	
	/**
	 * Returns the hash of the player's password.
	 * 
	 * @return a base64 SHA512 hash
	 * 
	 * @see #setHash(String)
	 * @see #setPwd(String)
	 */
	public String getHash() {
		return getData().getHash();
	}
	
	/**
	 * Sets the last ip the player logged with.<br>
	 * 
	 * @param ip the client's connection (ex: "255.255.255.255").
	 * 
	 * @see AuthData#getLastIP()
	 */
	public void setLastIP(String ip) {
		getData().setLastIP(ip);
	}
	
	/**
	 * returns a string representing the last player's connection.<br>
	 * 
	 * if not set, returns "".
	 * 
	 * @return the last client's ip
	 * 
	 * @see AuthData#setLastIP(String)
	 */
	public String getLastIP() {
		return getData().getLastIP();
	}
	
	/**
	 * Sets the player's last logged timestamp.<br>
	 * 
	 * @param inst the Instant instance.
	 * 
	 * @see AuthData#getLastLog()
	 * @see AuthData#setLastLog(long)
	 */
	public void setLastLog(Instant inst) {
		getData().setLastLog(inst);
	}
	
	/**
	 * Sets the player's last logged timestamp.<br>
	 * 
	 * @param inst the long representing the timestamp.
	 * 
	 * @see AuthData#setLastLog(Instant)
	 * @see AuthData#getLastLog()
	 */
	public void setLastLog(long inst) {
		getData().setLastLog(inst);
	}
	
	/**
	 * Returns the player's last logged timestamp.<br>
	 * 
	 * if not set, returns 0 (january 1st, 1970, 00:00:00)<br>
	 * 
	 * @return the long representing the timestamp.
	 * 
	 * @see AuthData#setLastLog(Instant)
	 * @see AuthData#setLastLog(long)
	 */
	public long getLastLog() {
		return getData().getLastLog();
	}

	/**
	 * Sets the player's logged status.<br>
	 * 
	 * @param logged the player's new logged status
	 */
	public void setLogged(boolean logged) {
		getData().setLogged(logged);
	}
	
	/**
	 * Sets the player's logged status to true and interrupts the kick timeout.
	 */
	public void setLogged() {
		setLogged(true);
		interruptTimeout();
	}
	
	/**
	 * Returns the player's logged status.<br>
	 * 
	 * @return the logged status
	 */
	public boolean getLogged() {
		return getData().getLogged();
	}
	
	/**
	 * Sets the player's timeout.<br>
	 * 
	 * <strong>The timeout must be the kick timeout</strong><br>
	 * 
	 * @param func the kick function
	 * @param dt the delay before the /kick
	 */
	public void setTimeout(Runnable func, int dt) {
		getData().setTimeout(func,  dt);
	}
	
	/**
	 * Interrupt the player's timeout.<br>
	 */
	public void interruptTimeout() {
		getData().interruptTimeout();
	}
	
	/**
	 * Returns the player's timeout object.<br>
	 * 
	 * @return a Timeout Object
	 */
	public Task getTimeout() {
		return getData().getTimeout();
	}
}
