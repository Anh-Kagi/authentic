package org.framagit.anhkagi.authentic.util;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

/**
 * Utility class for password hashing and verification.
 * 
 * @author Anh-Kagi
 */
public class Password {
	/**
	 * Hashes the password into SHA-512 base64 string.<br>
	 * 
	 * TODO: change to SHA3-512/Keccak-512
	 * 
	 * @param password the password to hash
	 * @return the base64 hashed string
	 * 
	 * @see Base64
	 * @see MessageDigest
	 */
	public static String hash(String password) {
		try {
			return Base64.getEncoder()
					.encodeToString(MessageDigest.getInstance("SHA-512")
							.digest(password.getBytes(StandardCharsets.UTF_8))); 
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			return null;
		}
		
	}
	
	/**
	 * compare the hashed value returned by the given password and the given hash.<br>
	 * 
	 * @param hash the hash to compare to
	 * @param password the password to hash
	 * @return true if the hash corresponds to the password, false otherwise
	 */
	public static boolean verify(String hash, String password) {
		return hash.equals(hash(password));
	}
}
