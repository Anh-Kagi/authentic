package org.framagit.anhkagi.authentic.command;

import org.framagit.anhkagi.authentic.util.AuthData;
import org.framagit.anhkagi.authentic.util.L10n;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.args.GenericArguments;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.entity.living.player.User;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

/**
 * The command executor for the /logged command.<br>
 * 
 * Displays the status of the player (logged or not).<br>
 * 
 * Can target a player.<br>
 * 
 * @author Anh-Kagi
 */
public class Logged implements CommandExecutor {
	/**
	 * The command's name.<br>
	 */
	public final static String command = "logged";

	/**
	 * The function executed when running /logged<br>
	 * 
	 * displays the corresponding strings stored in L10n following the result.<br>
	 * 
	 * @see CommandSource
	 * @see CommandContext
	 * @see CommandResult
	 * @see AuthData
	 * @see L10n
	 * @see L10n#logStatusTrue
	 * @see L10n#logStatusFalse
	 * @see L10n#playerLogStatusTrue
	 * @see L10n#playerLogStatusFalse
	 */
	@Override
	public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {
		User target = args.<Player>getOne("target").orElse(null);
		if (src instanceof Player)
			if (target == null) {
				if (AuthData.getPlayer(src.getName()).getLogged())
					src.sendMessage(Text.of(TextColors.GREEN, L10n.logStatusTrue));
				else
					src.sendMessage(Text.of(TextColors.RED, L10n.logStatusFalse));
				
				return CommandResult.builder()
						.affectedEntities(1)
						.successCount(1)
						.build();
			} else {
				if (AuthData.getPlayer(src.getName()).getLogged())
					src.sendMessage(Text.of(TextColors.GREEN, String.format(L10n.playerLogStatusTrue, target.getName())));
				else
					src.sendMessage(Text.of(TextColors.RED, String.format(L10n.playerLogStatusFalse, target.getName())));
	
				return CommandResult.builder()
						.affectedEntities(1)
						.successCount(1)
						.build();
			}
		else
			return CommandResult.builder()
					.affectedEntities(0)
					.successCount(1)
					.build();
	}
	
	/**
	 * Builds the command using CommandSpec.<br>
	 * 
	 * @return the command specification
	 * 
	 * @see CommandSpec
	 * @see L10n#commandLoggedDesc
	 */
	public static CommandSpec spec() {
		return CommandSpec.builder()
				.description(Text.of("/"+command+" [<player>]"))
				.extendedDescription(Text.of("/"+command+" [<player>] - " + L10n.commandLoggedDesc))
				.arguments(GenericArguments.optional(GenericArguments.user(Text.of("target"))))
				.executor(new Logged())
				.build();
	}
}
