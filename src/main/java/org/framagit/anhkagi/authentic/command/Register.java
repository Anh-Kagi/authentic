package org.framagit.anhkagi.authentic.command;

import org.framagit.anhkagi.authentic.Authentic;
import org.framagit.anhkagi.authentic.util.AuthData;
import org.framagit.anhkagi.authentic.util.L10n;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.args.GenericArguments;
import org.spongepowered.api.command.source.CommandBlockSource;
import org.spongepowered.api.command.source.ConsoleSource;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

/**
 * The command executor for the /register command.<br>
 * 
 * Allows to register or to change his password.<br>
 * 
 * @author Anh-Kagi
 */
public class Register implements CommandExecutor {
	/**
	 * The command's name.<br>
	 */
	public final static String command = "register";

	/**
	 * The functions executed when doing /register.<br>
	 * 
	 * Sets the password for the player.<br>
	 * 
	 * @see CommandSource
	 * @see CommandContext
	 * @see CommandResult
	 * @see AuthData
	 * @see L10n#playerJustRegistered
	 * @see L10n#logStatusTrue
	 * @see L10n#playerJustChangedPwd
	 * @see L10n#passwordSet
	 * @see L10n#errorNotLogged
	 * @see L10n#errorPwdTooShort
	 * @see L10n#warnPwdDiff
	 * @see L10n#consoleWantsToLog
	 * @see L10n#commandblockWantsToLog
	 * @see L10n#errorUnableToLog
	 */
	@Override
	public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {
		if (src instanceof Player) {
			// wether the two passwords are identical
			if (args.<String>getOne("password").get().equals(args.<String>getOne("repeat_pwd").get())) {
				// wether the password is long enough
				if (args.<String>getOne("password").get().length() >= Authentic.config.getNode("minimumPwdSize").getInt()) {
					AuthData auth = AuthData.getPlayer(src.getName());
					if (auth.getHash() == null) {
						src.sendMessage(Text.of(TextColors.RED, L10n.errorDB));
						
						return CommandResult.builder()
								.affectedEntities(1)
								.successCount(0)
								.build();
					} else if (auth.getHash().equals("")) { // no password set (not registered)
						Authentic.log(String.format(L10n.playerJustRegistered, src.getName()));
						src.sendMessage(Text.of(TextColors.GREEN, L10n.logStatusTrue));
						
						// change password and set logged
						auth.setPwd(args.<String>getOne("password").get());
						auth.setLogged();
						
						return CommandResult.builder()
								.affectedEntities(1)
								.successCount(1)
								.build();
					} else {
						if (AuthData.getPlayer(src.getName()).getLogged()) { // if registered (change password)
							auth.setPwd(args.<String>getOne("password").get());
							
							Authentic.log(String.format(L10n.playerJustChangedPwd, src.getName()));
							src.sendMessage(Text.of(TextColors.GREEN, L10n.passwordSet));
							
							return CommandResult.builder()
									.affectedEntities(1)
									.successCount(1)
									.build();
						} else { // not connected but password set (attempt to change the password without being connected)
							src.sendMessage(Text.of(TextColors.RED, L10n.errorNotLogged));
							
							return CommandResult.builder()
									.affectedEntities(1)
									.successCount(0)
									.build();
						}
					}
				} else { // password too short
					src.sendMessage(Text.of(TextColors.RED, String.format(L10n.errorPwdTooShort, Authentic.config.getNode("minimumPwdSize").getString())));
					
					return CommandResult.builder()
							.affectedEntities(1)
							.successCount(0)
							.build();
				}
			} else { // the two passwords are differents
				src.sendMessage(Text.of(TextColors.RED, L10n.warnPwdDiff));
				
				return CommandResult.builder()
						.affectedEntities(1)
						.successCount(0)
						.build();
			}
		} else if (src instanceof ConsoleSource) {
			src.sendMessage(Text.of(TextColors.RED, L10n.consoleWantsToLog));
			
			return CommandResult.builder()
					.affectedEntities(0)
					.successCount(0)
					.build();
		} else if (src instanceof CommandBlockSource) {
			src.sendMessage(Text.of(TextColors.YELLOW, L10n.commandblockWantsToLog));
			
			return CommandResult.builder()
					.affectedBlocks(1)
					.successCount(0)
					.build();
		} else
			throw new CommandException(Text.of(TextColors.RED, L10n.errorUnableToLog));
	}

	/**
	 * Builds the command using CommandSpec.<br>
	 * 
	 * @return the command specification
	 * 
	 * @see CommandSpec
	 * @see L10n#commandRegisterDesc
	 */
	public static CommandSpec spec() {
		return CommandSpec.builder()
				.description(Text.of("/"+command+" <password> <password>"))
				.extendedDescription(Text.of("/"+command+" <password> <password> - "+L10n.commandRegisterDesc))
				.executor(new Register())
				.arguments(GenericArguments.onlyOne(GenericArguments.string(Text.of("password"))),
						GenericArguments.onlyOne(GenericArguments.string(Text.of("repeat_pwd"))))
				.build();
	}
}
