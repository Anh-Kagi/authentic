package org.framagit.anhkagi.authentic.command;

import org.framagit.anhkagi.authentic.Authentic;
import org.framagit.anhkagi.authentic.util.AuthData;
import org.framagit.anhkagi.authentic.util.L10n;
import org.framagit.anhkagi.authentic.util.Password;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.args.GenericArguments;
import org.spongepowered.api.command.source.CommandBlockSource;
import org.spongepowered.api.command.source.ConsoleSource;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

/**
 * The command executor for the /login command.<br>
 * 
 * Allows to login if already registered.<br>
 * 
 * @author Anh-Kagi
 */
public class Login implements CommandExecutor {
	/**
	 * The command's name.<br>
	 */
	public final static String command = "login";
	
	/**
	 * The function executed when doing /login.<br>
	 * 
	 * Sets the logged status to true if the given password matches the registered password.<br>
	 * 
	 * @see CommandSource
	 * @see CommandContext
	 * @see CommandResult
	 * @see AuthData
	 * @see L10n#playerJustLogged
	 * @see L10n#logStatusTrue
	 * @see L10n#wrongPassword
	 * @see L10n#warnNotRegistered
	 * @see L10n#consoleWantsToLog
	 * @see L10n#commandblockWantsToLog
	 * @see L10n#errorUnableToLog
	 */
	@Override
	public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {
		if (src instanceof Player) {
			AuthData auth = AuthData.getPlayer(src.getName());
			if (auth.getHash() == null) {
				src.sendMessage(Text.of(TextColors.RED, L10n.errorDB));
				
				return CommandResult.builder()
						.affectedEntities(1)
						.successCount(0)
						.build();
			} else if (!auth.getHash().equals("")) // the player is registered
				if (Password.verify(auth.getHash(), args.<String>getOne("password").get())) { // if password match
					Authentic.log(String.format(L10n.playerJustLogged, src.getName()));
					src.sendMessage(Text.of(TextColors.GREEN, L10n.logStatusTrue));
					
					auth.setLogged();
					
					return CommandResult.builder()
							.affectedEntities(1)
							.successCount(1)
							.build();
				} else { // passwords are differents
					Text msg = Text.of(TextColors.RED, L10n.wrongPassword);
					if (Authentic.config.getNode("doKickOnWrongPwd").getBoolean()) {
						((Player) src).kick(msg);
						
						return CommandResult.builder()
								.affectedEntities(1)
								.successCount(0)
								.build();
					}
					src.sendMessage(msg);
					
					return CommandResult.builder()
							.affectedEntities(1)
							.successCount(0)
							.build();
				}
			else { // player is not registered
				src.sendMessage(Text.of(TextColors.RED, L10n.warnNotRegistered));
				
				return CommandResult.builder()
						.affectedEntities(1)
						.successCount(0)
						.build();
			}
		} else if (src instanceof ConsoleSource) { // if executed from the console
			src.sendMessage(Text.of(TextColors.RED, L10n.consoleWantsToLog));
			
			return CommandResult.builder()
					.affectedEntities(0)
					.successCount(0)
					.build();
		} else if (src instanceof CommandBlockSource) { // if executed from the console
			src.sendMessage(Text.of(TextColors.YELLOW, L10n.commandblockWantsToLog));
			
			return CommandResult.builder()
					.affectedBlocks(1)
					.successCount(0)
					.build();
		} else // in other cases
			throw new CommandException(Text.of(TextColors.RED, L10n.errorUnableToLog));
	}
	
	/**
	 * Builds the command using CommandSpec.<br>
	 * 
	 * @return the command specification
	 * 
	 * @see CommandSpec
	 * @see L10n#commandLoginDesc
	 */
	public static CommandSpec spec() {
		return CommandSpec.builder()
				.description(Text.of("/"+command+" <password>"))
				.extendedDescription(Text.of("/"+command+" <password> - "+L10n.commandLoginDesc))
				.arguments(GenericArguments.onlyOne(GenericArguments.string(Text.of("password"))))
				.executor(new Login())
				.build();
	}
}
