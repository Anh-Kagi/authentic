package org.framagit.anhkagi.authentic;

import java.io.IOException;
import org.framagit.anhkagi.authentic.util.L10n;
import org.slf4j.Logger;
import org.spongepowered.api.plugin.PluginContainer;

import ninja.leaping.configurate.ConfigurationNode;
import ninja.leaping.configurate.ConfigurationOptions;
import ninja.leaping.configurate.commented.CommentedConfigurationNode;
import ninja.leaping.configurate.hocon.HoconConfigurationLoader;
import ninja.leaping.configurate.loader.ConfigurationLoader;

/**
 * The wrapper class for the configuration file.
 * 
 * @author Anh-Kagi
 */
public class Config {
	private PluginContainer plugin;
	
	/**
	 * The setter for the plugin container object (given in the main class by SpongeApi).<br>
	 * 
	 * @param plugin the plugin container instance
	 * 
	 * @see PluginContainer
	 * @see Config#getPluginContainer()
	 */
	public void setPluginContainer(PluginContainer plugin) {
		this.plugin = plugin;
	}
	
	/**
	 * The getter for the plugin container instance.<br>
	 * 
	 * Used to get the default configuration file (located in assets/authentic/).
	 * 
	 * @return the plugin container instance
	 * 
	 * @see PluginContainer
	 * @see Config#setPluginContainer(PluginContainer)
	 */
	public PluginContainer getPluginContainer() {
		return plugin;
	}
	
	private ConfigurationLoader<CommentedConfigurationNode> loader;
	
	/**
	 * The setter for the configuration loader.
	 * 
	 * @param loader the ConfigurationLoader instance given by SpongeApi to the main class
	 * 
	 * @see ConfigurationLoader
	 * @see CommentedConfigurationNode
	 * @see Config#getConfigLoader()
	 */
	public void setConfigLoader(ConfigurationLoader<CommentedConfigurationNode> loader) {
		this.loader = loader;
	}
	
	/**
	 * The getter for the configuration loader.<br>
	 * 
	 * Used the save the configuration file (player's data, missing data...).
	 * 
	 * @return the configuration loader instance
	 * 
	 * @see ConfigurationLoader
	 * @see CommentedConfigurationNode
	 * @see Config#setConfigLoader(ConfigurationLoader)
	 */
	public ConfigurationLoader<CommentedConfigurationNode> getConfigLoader() {
		return loader;
	}
	
	private ConfigurationNode config;
	
	/**
	 * The getter for the configuration file.<br>
	 * 
	 * Used to access to the different nodes set in the file.
	 * 
	 * @return the configuration file instance
	 * 
	 * @see ConfigurationNode
	 */
	public ConfigurationNode getConfig() {
		return config;
	}

	
	/**
	 * The constructor of the class.
	 * 
	 * @param p the plugin container given by SpongeApi
	 * @param l the config loader given by SpongeApi
	 * 
	 * @see PluginContainer
	 * @see ConfigurationLoader
	 * @see CommentedConfigurationNode
	 * @see Logger
	 * @see Config#getPluginContainer()
	 * @see Config#getConfigLoader()
	 */
	public Config(PluginContainer p, ConfigurationLoader<CommentedConfigurationNode> l) {
		setPluginContainer(p);
		setConfigLoader(l);
	}
	
	/**
	 * Used to access to the nodes contained in the configuration file.<br>
	 * 
	 * The path given is relative to the first node ("Authentic").
	 * 
	 * @param path the path to the desired node
	 * @return the wanted node
	 * 
	 * @see ConfigurationNode
	 */
	public ConfigurationNode getNode(Object... path) {
		return getConfig().getNode("Authentic").getNode(path);
	}
	
	/**
	 * Loads the nodes from the configuration file given in the constructor.<br>
	 * 
	 * Writes to the Logger instance the result using the L10n strings.
	 * 
	 * @see Config#Config(PluginContainer, ConfigurationLoader)
	 * @see Config#getConfig()
	 * @see Config#getConfigLoader()
	 * @see L10n#configLoaded
	 * @see L10n#configDefaultLoaded
	 * @see L10n#errorConfigLoad
	 */
	public void load() {
		try {
			config = getConfigLoader().load();
			Authentic.log(L10n.configLoaded);
			
			config.mergeValuesFrom(HoconConfigurationLoader.builder()
					.setURL(getPluginContainer().getAsset("authentic.conf").get().getUrl())
					.build()
					.load(ConfigurationOptions.defaults().setShouldCopyDefaults(true)));
			Authentic.log(L10n.configDefaultLoaded);
			
			getConfigLoader().save(config);
		} catch (IOException e) {
			Authentic.error(L10n.errorConfigLoad);
			e.printStackTrace();
		}
	}
	
	public boolean isValid() {
		boolean success = true;
		
		// allowRegister
		if (!(getNode("allowRegister").getValue() instanceof Boolean)) {
			Authentic.error("allowRegister is not a boolean");
			success = false;
		}
		
		// timeOutDelay
		if (!(getNode("timeOutDelay").getValue() instanceof Integer)) { // doesn't automatically parse long
			Authentic.error("timeOutDelay is not an Integer");
			success = false;
		} else if (getNode("timeOutDelay").getInt() < 0) {
			Authentic.error("timeOutDelay is negative");
			success = false;
		}
		
		// immobilizePlayers
		if (!(getNode("immobilizePlayers").getValue() instanceof Boolean)) {
			Authentic.error("immobilizePlayers is not a Boolean");
			success = false;
		}
		
		// minimumPwdSize
		if (!(getNode("minimumPwdSize").getValue() instanceof Integer)) {
			Authentic.error("minimumPwdSize is not an integer");
			success = false;
		} else if (getNode("minimumPwdSize").getInt() < 1) {
			Authentic.error("minimumPwdSize is below 1");
			success = false;
		}
		
		// doKickOnWrongPwd
		if (!(getNode("doKickOnWrongPwd").getValue() instanceof Boolean)) {
			Authentic.error("doKickOnWrongPwd is not a Boolean");
			success = false;
		}
		
		// logCacheDelay
		if (!(getNode("logCacheDelay").getValue() instanceof Integer)) {
			Authentic.error("logCacheDelay is not an Integer");
			success = false;
		} else if (getNode("logCacheDelay").getInt() < 0) {
			Authentic.error("logCacheDelay is negative");
			success = false;
		}
		
		// invulnerablePlayers
		if (!(getNode("invulnerablePlayers").getValue() instanceof Boolean)) {
			Authentic.error("invulnerablePlayers is not a Boolean");
			success = false;
		}
		
		// allowCommands
		if (!(getNode("allowCommands").getValue() instanceof Boolean)) {
			Authentic.error("allowCommands is not a Boolean");
			success = false;
		}
		
		// allowDropItems
		if (!(getNode("allowDropItems").getValue() instanceof Boolean)) {
			Authentic.error("allowDropItems is not a Boolean");
			success = false;
		}
		
		// DB
		// type
		if (getNode("DB", "type").getString().equals("MYSQL")) {
			// TODO check uri's validity
			
		} else if (!getNode("DB", "type").getString().equals("H2")) {
			Authentic.error("DB.type is neither \"H2\" nor \"MYSQL\"");
			success = false;
		}
		
		return success;
	}
}
