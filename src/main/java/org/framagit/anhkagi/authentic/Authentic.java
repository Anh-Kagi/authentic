package org.framagit.anhkagi.authentic;

import java.io.IOException;
import java.nio.file.Path;
import java.time.Instant;

import org.framagit.anhkagi.authentic.command.Logged;
import org.framagit.anhkagi.authentic.command.Login;
import org.framagit.anhkagi.authentic.command.Register;
import org.framagit.anhkagi.authentic.util.AuthData;
import org.framagit.anhkagi.authentic.util.L10n;

import org.slf4j.Logger;

import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.CommandManager;
import org.spongepowered.api.config.DefaultConfig;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.action.InteractEvent;
import org.spongepowered.api.event.cause.entity.damage.source.EntityDamageSource;
import org.spongepowered.api.event.command.SendCommandEvent;
import org.spongepowered.api.event.entity.DamageEntityEvent;
import org.spongepowered.api.event.entity.MoveEntityEvent;
import org.spongepowered.api.event.game.state.GameInitializationEvent;
import org.spongepowered.api.event.game.state.GamePreInitializationEvent;
import org.spongepowered.api.event.item.inventory.ClickInventoryEvent;
import org.spongepowered.api.event.item.inventory.DropItemEvent;
import org.spongepowered.api.event.network.ClientConnectionEvent;
import org.spongepowered.api.plugin.Plugin;
import org.spongepowered.api.plugin.PluginContainer;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

import com.google.inject.Inject;

import ninja.leaping.configurate.ConfigurationNode;
import ninja.leaping.configurate.hocon.HoconConfigurationLoader;

/**
 * The main class of Authentic plugin.<br>
 * 
 * Contains the event listeners needed for Authentic to work.
 * 
 * @author Anh-Kagi
 */
// TODO add custom events
// TODO check about permissions
@Plugin(id = "authentic",
		name = "Authentic",
		version = "1.4.2",
		description = "Authentication plugin for Sponge",
		url = "https://framagit.org/Anh-Kagi/authentic",
		authors = "Anh Kagi")
public class Authentic {
	@Inject
	private PluginContainer plugin;
	
	@Inject
	@DefaultConfig(sharedRoot = false)
	private Path configDir;
	
	/**
	 * The configuration file wrapper.
	 * 
	 * @see Config
	 */
	public static Config config;
	
	private static Logger logger;
	
	/**
	 * Shortcut for external classes to access to the Logger instance.<br>
	 * 
	 * Uses Logger.info().<br>
	 * 
	 * @param text The string to display in the console.
	 * 
	 * @see Logger
	 */
	public static void log(String text) {
		logger.info(text);
	}
	
	/**
	 * Shortcut for external classes to access to the Logger instance.<br>
	 * 
	 * Uses Logger.warn().<br>
	 * 
	 * @param text The string to display in the console
	 * 
	 * @see Logger
	 */
	public static void warn(String text) {
		logger.warn(text);
	}
	
	/**
	 * Shortcut for external classes to access to the Logger instance.<br>
	 * 
	 * Uses Logger.error().<br>
	 * 
	 * @param text The string to display in the console
	 * 
	 * @see Logger
	 */
	public static void error(String text) {
		logger.error(text);
	}
	
	/**
	 * Constructor.<br>
	 * 
	 * Injects the Logger instance to the static variable.<br>
	 * 
	 * @param logger The Logger instance given by SpongeAPI
	 * 
	 * @see Logger
	 */
	@Inject
	public Authentic(Logger logger) {
		Authentic.logger = logger;
	}
	
	/**
	 * The listener for the <code>GamePreInitializationEvent</code>.<br>
	 * Loads the configuration file and the localization (L10n) strings.<br>
	 * Copies the db file if none have been found.<br>
	 * Sets up the jdbc string.<br>
	 * 
	 * @param event the GamePreInitializationEvent given by SpongeApi
	 * 
	 * @see GamePreInitializationEvent
	 * @see L10n#preInit
	 * @see L10n#dbPointsTo
	 * @see AuthData
	 */
	@Listener
	public void onPreInit(GamePreInitializationEvent event) {
		// loading configuration file
		config = new Config(plugin,
				HoconConfigurationLoader.builder()
				.setPath(configDir).build());
		config.load();
		
		if (!config.isValid()) {
			error(L10n.configInvalid);
			Sponge.getServer().shutdown(Text.of(TextColors.DARK_RED, L10n.configInvalidShutdown));
			// TODO find a better way to stop server on error
		}
		log(L10n.configValid);
		
		// let's make configDir point to the actual configuration directory
		// (@DefaultConfig is used to create an empty .conf file by itself)
		configDir = configDir.getParent();
		
		// loading L10n strings
		L10n.load(config.getNode("L10n"));
		
		log(L10n.preInit);
		
		AuthData.plugin = plugin;
		
		// sets the sql table that AuthData must use
		switch (config.getNode("DB", "type").getString()) {
		case "H2":
			AuthData.uri = "jdbc:h2:./"+Sponge.getGame().getGameDirectory().toAbsolutePath().relativize(configDir).resolve("db");
			AuthData.table = "authentic";
			
			// copy the empty db file to the configuration directory (if no file have been found)
			if (!configDir.resolve("db.mv.db").toFile().exists())
				try {
					plugin.getAsset("db.mv.db").get().copyToDirectory(configDir);
				} catch (IOException e) {
					Sponge.getServer().shutdown(Text.of(TextColors.DARK_RED, L10n.errorDBFile));
				}
			break;
		case "MYSQL":
			ConfigurationNode dbNode = config.getNode("DB");
			String host, port, user, pwd, database, table;
			host = dbNode.getNode("host").getString();
			port = dbNode.getNode("port").getString();
			user = dbNode.getNode("user").getString();
			pwd = dbNode.getNode("pwd").getString();
			database = dbNode.getNode("database").getString();
			table = dbNode.getNode("table").getString();
			
			AuthData.uri = "jdbc:mysql://"+user+(pwd.equals("") ? "" : ":"+pwd)+"@"+host+":"+port+"/"+database;
			AuthData.table = table;
			break;
		default:
			// This line isn't really useful, as the configuration file would be invalid
			// and the server's startup would have been stoppped before.
			Sponge.getServer().shutdown(Text.of(TextColors.DARK_RED, L10n.configInvalidShutdown));
		}
		
		// displays the db uri in the console
		log(String.format(L10n.dbPointsTo, AuthData.uri));
	}
	
	/**
	 * The listener for the <code>GameInitializationEvent</code>.<br>
	 * Registers the commands (/login, /register, /logged).
	 * 
	 * @param event the GameInitializationEvent given by SpongeApi
	 * 
	 * @see GameInitializationEvent
	 * @see L10n#init
	 * @see L10n#registeringCommand
	 * @see L10n#skipRegistration
	 */
	@Listener
	public void onInit(GameInitializationEvent event) {
		CommandManager cm = Sponge.getCommandManager();
		
		log(L10n.init);
		
		// register /login
		cm.register(this, Login.spec(), Login.command);
		log(String.format(L10n.registeringCommand, "login"));
		
		// register /register
		if (config.getNode("allowRegister").getBoolean()) {
			cm.register(this, Register.spec(), Register.command);
			log(String.format(L10n.registeringCommand, "register"));
		} else
			log(L10n.skipRegistration);
		
		// register /logged
		cm.register(this, Logged.spec(), "logged");
		log(String.format(L10n.registeringCommand, Logged.command));
	}
	
	/**
	 * The listener fired when a player tries to authenticate to the server.<br>
	 * 
	 * Retrieves the data from the database, kindly kicks the player's if unable to.<br>
	 * 
	 * Players whose data is not accessible (inaccessible database and data not stored in memory) won't enter and get a message saying that the database is unavailable.<br> 
	 * 
	 * @param event the ClientConnectionEvent fired on player's authentication.
	 * 
	 * @see ClientConnectionEvent
	 * @see ClientConnectionEvent.Auth
	 */
	@Listener
	public void onPlayerAuth(ClientConnectionEvent.Auth event) {
		if (!AuthData.retrieve(event.getProfile().getName().get())) {
			event.setCancelled(true);
			event.setMessage(Text.of(TextColors.DARK_RED, "The server has issues with the database, you may not connect now."));
		}
	}
	
	/**
	 * The listener fired when a player joins the game.<br>
	 * Invites the player to login or register himself.<br>
	 * 
	 * It automatically connects the player if the last time he logged in was on this IP<br>
	 * and was within the last x seconds. Where x is the value of the "logCacheDelay" rule.<br>
	 * 
	 * @param event the ClientConnectionEvent fired on player's joining
	 * 
	 * @see ClientConnectionEvent
	 * @see ClientConnectionEvent.Join
	 * @see L10n#playerJoin
	 * @see L10n#welcomeLog
	 * @see L10n#welcomeReg
	 */
	@Listener
    public void onPlayerJoin(ClientConnectionEvent.Join event) {
		Player player = event.getTargetEntity();
		String name = player.getName();

		AuthData auth = AuthData.getPlayer(name);
		
		log(String.format(L10n.playerJoin, name));
		
		// if the player can skip the login (already connected in the last x seconds on the same ip
		boolean logCache = player.getConnection().getAddress().getAddress().getHostAddress().equals(auth.getLastIP()) ?
				Instant.now().getEpochSecond() - auth.getLastLog() < config.getNode("logCacheDelay").getInt() :
					false;
		
		if (!logCache) {
			if ("".equals(auth.getHash()))
				player.sendMessage(Text.of(TextColors.GREEN, L10n.welcomeReg));
			else
				player.sendMessage(Text.of(TextColors.GREEN, L10n.welcomeLog));
			
			// reset last connection
			auth.setLastIP("");
			auth.setLastLog(0);
			
			// sets the kicking timeout
			auth.setTimeout(() -> player.kick(Text.of(TextColors.DARK_RED, L10n.timeOut)),
					config.getNode("timeOutDelay").getInt());
		} else {
			player.sendMessage(Text.of(TextColors.GREEN, String.format(L10n.welcomeAlreadyLog, config.getNode("logCacheDelay").getInt())));
			
			auth.setLogged();
		}
    }
	
	/**
	 * The listener fired when a player quits the game.<br>
	 * Unsets the logged status of the player.<br>
	 * 
	 * Interrupts the timeout of the player if it exists.<br>
	 * 
	 * Syncs the player's data to the database, if it is inaccessible, the data will be kept in memory.<br>
	 * 
	 * @param event the ClientConnectionEvent fired on player's quitting.
	 * 
	 * @see ClientConnectionEvent
	 * @see ClientConnectionEvent.Disconnect
	 * @see L10n#playerQuit
	 */
	@Listener
	public void onPlayerQuit(ClientConnectionEvent.Disconnect event) {
		Player player = event.getTargetEntity();
		String name = player.getName();
		
		AuthData auth = AuthData.getPlayer(name);
		
		log(String.format(L10n.playerQuit, name));
		
		// sets the player's last logged time and ip
		if (auth.getLogged()) {
			auth.setLastLog(Instant.now().getEpochSecond());
			auth.setLastIP(player.getConnection().getAddress().getAddress().getHostAddress());
		}
		
		// set the player's status to unlogged.
		auth.setLogged(false);
		auth.interruptTimeout();
		
		// sync player's data to the database
		AuthData.save(name);
	}
	
	/**
	 * The event fired when an entity moves.<br>
	 * 
	 * It cancels the move if the player isn't logged in (and if the entity is a player).
	 * 
	 * @param event the MoveEntityEvent fired by SpongeApi when an entity moves.
	 * 
	 * @see MoveEntityEvent
	 * @see L10n#warnNotLogged
	 */
	@Listener
	public void onEntityMove(MoveEntityEvent event) {
		if (config.getNode("immobilizePlayers").getBoolean())
			if (event.getTargetEntity() instanceof Player) {
				Player player = (Player) event.getTargetEntity();
				
				if (!AuthData.getPlayer(player.getName()).getLogged()) {
					event.setCancelled(true); // cancels the event
					
					player.sendMessage(Text.of(TextColors.DARK_RED, L10n.warnNotLogged));
				}
			}
	}
	
	/**
	 * The event fired when an entity tries to interact with the world (breaking blocs, hitting entities...).<br>
	 * 
	 * It cancels the interaction if the player isn't logged in (and if the entity is a player).
	 * 
	 * @param event the InteractionEvent fired by SpongeApi when an entity attempts to interact with the world.
	 * 
	 * @see InteractEvent
	 * @see L10n#warnNotLogged
	 */
	@Listener
	public void onEntityInteract(InteractEvent event) {
		if (config.getNode("immobilizePlayers").getBoolean())
			if (event.getCause().root() instanceof Player) {
				Player player = (Player) event.getCause().root();
				
				if (!AuthData.getPlayer(player.getName()).getLogged()) {
					event.setCancelled(true);
					
					player.sendMessage(Text.of(TextColors.DARK_RED, L10n.warnNotLogged));
				}
			}
	}
	
	/**
	 * The event fired when an entity tries to draw damage to another entity.<br>
	 * 
	 * It cancels the interaction if either the target or the attacker is a non-logged player.
	 * 
	 * @param event the DamageEntityEvent fired by SpongeAPI when an entity tries to draw damage.
	 * 
	 * @see DamageEntityEvent
	 * @see L10n#warnTargetNotLogged
	 * @see L10n#warnNotLogged
	 */
	@Listener
	public void onEntityDamage(DamageEntityEvent event) {
		if (config.getNode("invulnerablePlayers").getBoolean())
			if (event.getTargetEntity() instanceof Player) {
				Player player = (Player) event.getTargetEntity();
				
				if (!AuthData.getPlayer(player.getName()).getLogged()) {
					event.setCancelled(true);
					 
					if (event.getSource() instanceof EntityDamageSource && ((EntityDamageSource) event.getSource()).getSource() instanceof Player)
						((Player) ((EntityDamageSource) event.getSource()).getSource()).sendMessage(Text.of(TextColors.DARK_RED, L10n.warnTargetNotLogged));
				}
			}
	}
	
	/**
	 * The event fired when a player tries to send a command.<br>
	 * 
	 * The command is cancelled if the sender is not logged in (and if the command isn't one of /login, /register or /logged).<br>
	 * 
	 * @param event the SendCommandEvent fired by SpongeAPI when a player tries to send a command.
	 * 
	 * @see SendCommandEvent
	 * @see L10n#warnNotLogged
	 */
	@Listener
	public void onPlayerCommand(SendCommandEvent event) {
		if (!config.getNode("allowCommands").getBoolean())
			if (event.getSource() instanceof Player && !AuthData.getPlayer(((Player) event.getSource()).getName()).getLogged()) {
				String command = event.getCommand();
				if (!command.equals(Login.command) && !command.equals(Logged.command) && !command.equals(Register.command)) {
						event.setCancelled(true);
						((Player) event.getSource()).sendMessage(Text.of(TextColors.DARK_RED, L10n.warnNotLogged));
				}
			}
	}
	
	/**
	 * The event fired when a player drops an itemstack.<br>
	 * 
	 * The event is cancelled if the player isn't logged.<br>
	 * 
	 * @param event the ClickInventoryEvent fired by SpongeAPI when a player tries to interact with its inventory.<br>
	 * 
	 * @see DropItemEvent
	 * @see L10n#warnNotLogged
	 */
	@Listener
	public void onPlayerDrop(ClickInventoryEvent event) {
		if (!config.getNode("allowDropItems").getBoolean())
			if (event.getSource() instanceof Player) {
				Player src = (Player) event.getSource();
				if (!AuthData.getPlayer(src.getName()).getLogged()) {
					event.setCancelled(true);
					src.sendMessage(Text.of(TextColors.DARK_RED, L10n.warnNotLogged));
				}
			}
	}
}
